<?php
namespace SHF\API;

use \Psr\Container\ContainerInterface;
use \Slim\Routing\RouteCollectorProxy as RouteCollectorProxy;

use \SHF\API\Services\Answer;
use \SHF\API\Services\Club;
use \SHF\API\Services\Incident;
use \SHF\API\Services\License;
use \SHF\API\Services\LicenseApplication;
use \SHF\API\Services\Person;
use \SHF\API\Services\PilotSkill;
use \SHF\API\Services\Question;
use \SHF\API\Services\ShfLogin;
use \SHF\API\Services\Skill;

use \Holmby\auth\Authenticate;
use \Holmby\CRUD\Config;
use \ShfConfig;

class App {
  private $app;

  public function __construct() {
    // set up DI container
    $app = \DI\Bridge\Slim\Bridge::create();
    $app->getRouteCollector()->setDefaultInvocationStrategy(new \Slim\Handlers\Strategies\RequestResponse());
    $container = $app->getContainer();
    $container->set(Authenticate::class, function (ContainerInterface $c) {
      return new Authenticate(\ShfConfig::PUBLIC_KEY, \ShfConfig::PRIVATE_KEY, \ShfConfig::JWT_LIFETIME);
    });
    $container->set(Config::class, function (ContainerInterface $c) {
      return new ShfConfig();
    });

    // configure routes
    $app->group('/shf/api/v1/', function (RouteCollectorProxy $group) {
      // login
      $group->post('login/',  [ShfLogin::class, 'login']);
      $group->get('login/', [ShfLogin::class, 'renewJWT']);

      // person
      $group->get('people/', [Person::class, 'readAll']);
      $group->get('person/{id}', [Person::class, 'read']);
//      $group->delete('person/{id}', [Person::class, 'delete']);
      $group->post('person/', [Person::class, 'create']);
      $group->put('person/{id}', [Person::class, 'replace']);

      // license
      $group->get('licenses/', [License::class, 'readAll']);

      // club
      $group->get('clubs/', [Club::class, 'readAll']);

      // skill
      $group->get('skills/', [Skill::class, 'readAll']);

      // pilot skill
      $group->get('pilot-skills/', [PilotSkill::class, 'readAll']);
      $group->post('pilot-skill/', [PilotSkill::class, 'create']);

      // question
      $group->get('questions/', [Question::class, 'readAll']);

      // answer
      $group->get('answers/', [Answer::class, 'readAll']);
      $group->post('answer/', [Answer::class, 'create']);
      $group->put('answer/{key}', [Answer::class, 'replace']);

      // renew licnese
      $group->get('license-application/{id}', [LicenseApplication::class, 'read']);
      $group->get('license-applications/', [LicenseApplication::class, 'readAll']);
      $group->post('license-application/', [LicenseApplication::class, 'create']);
      $group->put('license-application/{id}', [LicenseApplication::class, 'replace']);

      // incident
      $group->post('incidentreport/', [Incident::class, 'create']);
      $group->get('incidentreports/', [Incident::class, 'readAll']);

    });

    // configure middleware
    $app->add(new DecodeJsonMiddleware());
    $errorMiddleware = $app->addErrorMiddleware(true, true, true);
    $errorHandler = $errorMiddleware->getDefaultErrorHandler();
    $errorHandler->forceContentType('application/json');
    $this->app = $app;
  }

  public function getApp() {
    return $this->app;
  }
}

?>