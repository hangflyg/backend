<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpForbiddenException;

use \Holmby\CRUD\CRUD;

class License extends CRUD {
  const TABLE = 'shf_licens';
  const KEYS = array(
    'LicensNr' => 'licenseNbr'
  );
  const COLUMNS = array(
    'person_id' => 'personId'
  );

  public function authorizeRead(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    // office
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    // club admin
    if($jwt->privileges->clubadmin) {
      return;
    }
    throw new HttpForbiddenException($request, 'Unautorized read request. You do not have privileges to read license.');
  }
  public function authorizeReadAll(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    // office
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    // club admin
    if($jwt->privileges->clubadmin) {
      return;
    }
    throw new HttpForbiddenException($request, 'Unautorized read request. You do not have privileges to read licenses.');
  }
}
?>