<?php
namespace SHF\API\Services;

use \PDO;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Exception\HttpForbiddenException;

use Ramsey\Uuid\Uuid;

class Incident extends \Holmby\CRUD\CRUD {
  const TABLE = 'incident';
  const KEYS = array(
    'id' => 'id'
  );
  const COLUMNS = array(
    'severity' => 'severity',
    'reporterName' => 'reporterName',
    'reporterEmail' => 'reporterEmail',
    'reporterPhone' => 'reporterPhone',
    'timestampOfEvent' => 'timestampOfEvent',
    'typeOfFlight' => 'typeOfFlight',
    'description' => 'description',
    'licenseNbr' => 'licenseNbr',
    'club' => 'club',
    'age' => 'age',
    'licenseLevel' => 'licenseLevel',
    'totalFlightTime' => 'totalFlightTime',
    'resentFlightTime' => 'resentFlightTime',
    'gliderFlightTime' => 'gliderFlightTime',
    'damagePilot' => 'damagePilot',
    'damageGlider' => 'damageGlider',
    'glider' => 'glider',
    'harness' => 'harness',
    'location' => 'location',
    'locationDescription' => 'locationDescription',
    'commentReporter' => 'commentReporter',
    'createTimestamp' => 'createTimestamp',
    'lastUpdate' => 'lastUpdate'
  );

  const CREATE_COLUMNS = array(
    'id' => 'id',
    'severity' => 'severity',
    'reporterName' => 'reporterName',
    'reporterEmail' => 'reporterEmail',
    'reporterPhone' => 'reporterPhone',
    'timestampOfEvent' => 'timestampOfEvent',
    'typeOfFlight' => 'typeOfFlight',
    'description' => 'description',
    'licenseNbr' => 'licenseNbr',
    'club' => 'club',
    'age' => 'age',
    'licenseLevel' => 'licenseLevel',
    'totalFlightTime' => 'totalFlightTime',
    'resentFlightTime' => 'resentFlightTime',
    'gliderFlightTime' => 'gliderFlightTime',
    'damagePilot' => 'damagePilot',
    'damageGlider' => 'damageGlider',
    'glider' => 'glider',
    'harness' => 'harness',
    'location' => 'location',
    'locationDescription' => 'locationDescription',
    'commentReporter' => 'commentReporter'
  );

  public function authorizeReadAll(Request $request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'safty')) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized request ');
  }
  public function authorizeCreate(Request $request) {
    // anyone can create an incident report
  }
  public function authorizeRead($request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'safty')) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized request ');
  }
  public function authorizeReplace(Request $request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'safty')) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized replace request');
  }

  public function create(Request $request, Response $response) {
    $this->authorizeCreate($request);
    $body = $request->getParsedBody();
    $pdo = $this->connect();
    $columns = implode(',', array_keys($this::CREATE_COLUMNS));
    $values = implode(',:', array_keys($this::CREATE_COLUMNS));
    // TODO, this assumes one autoincrement primary key
    $query = 'insert into ' . $this::TABLE . '(' . $columns . ') Values (:' . $values . ')';
    $stm = $pdo->prepare($query);
    foreach($this::CREATE_COLUMNS as $db => $rest) {
      $stm->bindParam(':' . $db, $body[$rest], PDO::PARAM_STR);
    }
    $stm->execute();
    $id = $pdo->lastInsertId();
    $args = array('id' => $id);
    return $this->readUnauthorized($request, $response, $args)->withHeader('Cache-control', 'no-store')->withStatus(201, 'Created');
 }

 public function readUnauthorized(Request $request, Response $response, $args) {
  $selectNames = $this->makeSqlSelectPart($this::KEYS);
  if(!empty($this::KEYS) || !empty($this::COLUMNS)) {
    $selectNames .= ',';
  }
  $selectNames .= $this->makeSqlSelectPart($this::COLUMNS);
  $condition = $this->makeSqlNameBinding($this::KEYS, ' and ');
  $query = 'select ' . $selectNames
         . ' from ' . $this::TABLE
         . ' where ' . $condition;
  $pdo = $this->connect();
  $stm = $pdo->prepare($query);
  $this->bindValues($this::KEYS, $args, $stm);
  $stm->execute();
  $row = $stm->fetch();
  if(!$row) {
    throw new HttpNotFoundException($request, 'No matching person found.');
  }
  $payload = json_encode($row);
  $response->getBody()->write($payload);
  return $response->withHeader('Content-Type', 'application/json');
}

}
?>