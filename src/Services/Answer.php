<?php
namespace SHF\API\Services;

use \PDO;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Exception\HttpForbiddenException;

class Answer extends \Holmby\CRUD\CRUD {
  const TABLE = 'answers';
  const KEYS = array(
    'q_id' => 'qId',
    'year' => 'year',
    'person_id' => 'personId'
  );
  const COLUMNS = array(
    'value' => 'value',
    'text' => 'text'
  );

  public function authorizeReadAll(Request $request, $args) {
    // a person can allways read its own data
    $jwt = $this->auth->authenticateUser($request);
    $params = $request->getQueryParams();
    if(!$params['personId'] || $params['personId'] != $jwt->sub) {
      throw new HttpForbiddenException($request, 'unautorized request ' . $params['personId']);
    }
    return;
  }
  public function authorizeCreate(Request $request) {
    // a person can allways read its own data
    $jwt = $this->auth->authenticateUser($request);
    $body = $request->getParsedBody();
    if(!$body['personId'] || $body['personId'] != $jwt->sub) {
      throw new HttpForbiddenException($request, 'unautorized create request');
    }
  }
  public function authorizeReplace(Request $request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    $body = $request->getParsedBody();
    if(!$body['personId'] || $body['personId'] != $jwt->sub) {
      throw new HttpForbiddenException($request, 'unautorized replace request');
    }
  }

  public function readAll(Request $request, Response $response, $args) {
    $this->authorizeReadAll($request, $args);
    $query = 'select q_id AS qId,year,person_id AS personId,value,text '
           . ' from answers where person_id=:person_id';
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    $params = $request->getQueryParams();
    $stm->bindParam(':person_id', $params['personId']);
    $stm->execute();
    $result = $stm->fetchAll();
    $payload = json_encode($result);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function create(Request $request, Response $response) {
    $this->authorizeCreate($request);
    $body = $request->getParsedBody();
    $pdo = $this->connect();
    $columns = implode(',', array_keys($this::KEYS));
    $columns .= ',' . implode(',', array_keys($this::COLUMNS));
    $values = implode(',:', array_keys($this::KEYS));
    $values .= ',:' . implode(',:', array_keys($this::COLUMNS));
    $query = 'insert into ' . $this::TABLE . '(' . $columns . ') Values (:' . $values . ')';
    $stm = $pdo->prepare($query);
    foreach($this::COLUMNS as $db => $rest) {
      $stm->bindParam(':' . $db, $body[$rest], PDO::PARAM_STR);
    }
    foreach($this::KEYS as $db => $rest) {
      $stm->bindParam(':' . $db, $body[$rest], PDO::PARAM_STR);
    }
    $stm->execute();

    $payload = json_encode($body);
    $response->getBody()->write($payload);
    return $response->withHeader('Cache-control', 'no-store')->withStatus(201, 'Created');
  }

  /**
   * Execute a replace operation.
   * Calls authorizeReplace() to autorize the operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function replace(Request $request, Response $response, $args) {
    $this->authorizeReplace($request, $args);
    $body = $request->getParsedBody();
    $pdo = $this->connect();
    $values = $this->makeSqlNameBinding($this::COLUMNS);
    $keys = $this->makeSqlNameBinding($this::KEYS, ' and ');
    $query = 'update ' . $this::TABLE . ' set ' . $values . ' where ' . $keys;
    // TODO, this do not update the primary keys
    $stm = $pdo->prepare($query);
    $this->bindValues($this::KEYS, $body, $stm);
    $this->bindValues($this::COLUMNS, $body, $stm);
    $stm->execute();
    // TODO, check if a row is updated, if not return 204 (No Content)
    // TODO, this assumes that the primary keys are not updated
    $payload = json_encode($body);
    $response->getBody()->write($payload);
    return $response->withHeader('Cache-control', 'no-store');
  }

}
?>