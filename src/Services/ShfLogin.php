<?php
namespace SHF\API\Services;

use \Slim\Exception\HttpNotFoundException;
use \Slim\Exception\HttpNotImplementedException;

use \Holmby\Auth\Authenticate;
use \Holmby\Auth\LoginTemplate;
use \Holmby\CRUD\Database;

class Dummy {
}

class ShfLogin extends LoginTemplate {
  protected $db;
  protected $auth;

  public function __construct(Authenticate $auth, Database $db) {
    parent::__construct($auth);
    $this->db = $db;
  }
  
  /**
   * Verify a (user, password) pair.
   * Throws an exception if the credentials are bad.
   * $user - personnummer
   * $password - licensnummer
   * return - array with id, givenName, familyName, and email of the user
   */
  protected function validateUserPassword($user, $password, $request) {
    // check that the server support bcrypt
    if (!(defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH)) {
      throw new HttpNotImplementedException($request, 'Support for CRYPT_BLOWFISH is missing on the server.');
    }
  
    $pdo = $this->db->connect();
    $authArray = $this->pnrLogin($pdo, $user, $password);
    if(!$authArray) {
      throw new HttpNotFoundException($request, 'Okänd användare eller felaktigt lösenord.');
    }
    return $authArray;
  }

  private function pnrLogin($pdo, $pnr, $licNbr) {
    $query = 'SELECT Id as id, FNamn as givenName, ENamn as familyName, email, shf_licens.LicensNr as licenseNbr FROM medlemmar, shf_licens '
      . ' WHERE shf_licens.PersonNr=medlemmar.PersonNr '
      . ' AND shf_licens.PersonNr=:pnr'
      . ' AND shf_licens.LicensNr=:licNr LIMIT 1';
    $stm = $pdo->prepare($query);
    $stm->bindParam(':pnr', $pnr, \PDO::PARAM_STR);
    $stm->bindParam(':licNr', $licNbr, \PDO::PARAM_INT);
    $stm->execute();
    $authArray = $stm->fetch();
    return $authArray;
  }

  /**
   * Return an object, a dictionary with the user priovileges.
   * The dictionary keys are the privileges.
   * The dictionary values are arrays containing the resources associated with the privilege.
   * example: { "clubadmin": [11], "office": []}
   */
  protected function getPrivileges($authArray) {
    $privileges = new Dummy();
    $pdo = $this->db->connect();
    $query = 'select privilege, resource from user_privileges where person_id=:pid';
    $stmt = $pdo->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
    $stmt->bindParam(':pid', $authArray['id'], \PDO::PARAM_STR);
    $stmt->execute();
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC, \PDO::FETCH_ORI_NEXT)) {
      $priv = $row['privilege'];
      $res = $row['resource'];
      // add the proivilege as a key in the object, with no associated resource
      if(!isset($privileges->$priv)) {
        $privileges->$priv = array();
      }
      // add the resource to array if it exists, else keep an empty array
      if($res != NULL) {
        $privileges->$priv[] = $res;
      }
    }
    return $privileges;
  }

  protected function generateJwtContent($user_array) {
    $jwt_array = parent::generateJwtContent($user_array);
    $jwt_array['lic'] = $user_array['licenseNbr'];
    return $jwt_array;
  }

}
?>
