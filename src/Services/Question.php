<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;

class Question extends \Holmby\CRUD\CRUD {
  const TABLE = 'questions';
  const KEYS = array(
    'q_id' => 'id'
  );
  const COLUMNS = array(
    'text' => 'text',
    'answer_type' => 'answerType'
  );

  public function authorizeReadAll(Request $request, $args){
    return;
  }
}
?>