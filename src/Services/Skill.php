<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpForbiddenException;

use \Holmby\CRUD\CRUD;

class Skill extends CRUD {
  const TABLE = 'skills';
  const KEYS = array(
    'skill_id' => 'id'
  );
  const COLUMNS = array(
    'swedish' => 'swedish',
    'english' => 'english'
  );

  public function authorizeReadAll(Request $request, $args){
    return;
  }
}
?>