<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Exception\HttpForbiddenException;
use \Slim\Exception\HttpNotFoundException;
use \PDO;

use \Holmby\CRUD\CRUD;

class LicenseApplication extends CRUD {
  const TABLE = 'shf_renew_lic';
  const KEYS = array(
    'id' => 'id'
  );
  const COLUMNS = array(
    'LicensNr' => 'licenseNbr',
    'year' => 'year',
    'level' => 'level',
    'club_id' => 'clubId',
    'downgrade_elev' => 'downgradeToElev',
    'need_flight_test' => 'needFlightTest',
    'timestamp_pilot' => 'timestampPilot',
    'timestamp_club' => 'timestampClub',
    'approved_club' => 'approvedClub',
    'timestamp_office' => 'timestampOffice',
    'approved_office' => 'approvedOffice',
    'timestamp_instructor' => 'timestampInstructor',
    'comment' => 'comment',
    'comment_office' => 'commentOffice'
  );

  public function authorizeReplace(Request $request, $args){
    // a pilot can allways read its own data
    $jwt = $this->auth->authenticateUser($request);
    if($jwt->privileges->clubadmin) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized read request');
  }

  public function authorizeReadAll(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'clubadmin')) {
      return;
    }
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }

    // a pilot can allways read its own data
    $params = $request->getQueryParams();
    if($params['licenseNbr'] && $params['licenseNbr'] == $jwt->lic) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized read request');
  }

  public function authorizeCreate(Request $request) {
    // a person can allways read its own data
    $jwt = $this->auth->authenticateUser($request);
    $body = $request->getParsedBody();
    if(!$body['licenseNbr'] || $body['licenseNbr'] != $jwt->lic) {
      throw new HttpForbiddenException($request, 'unautorized read request');
    }
    return;
  }

  /**
   * Execute a replace operation.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function replace(Request $request, Response $response, $args) {
    // Quick and derty, only works for clubs deciding on an application
    $pdo = $this->connect();
    $body = $request->getParsedBody();
    $jwt = $this->auth->authenticateUser($request);
    //--- club ---
    if (($jwt->privileges->clubadmin) && array_key_exists('approvedClub', $body)) {
      $query = 'update shf_renew_lic set approved_club=:decision, timestamp_club=now() where id=:id';
      $stm = $pdo->prepare($query);
      $stm->bindParam(':decision', $body['approvedClub'], PDO::PARAM_STR);
      $stm->bindParam(':id', $args['id'], PDO::PARAM_STR);
      $stm->execute();
      // TODO check if the row existed, if not return 404
    }
    //--- office ---
    if (property_exists($jwt->privileges, 'office') && array_key_exists('approvedOffice', $body)) {
      $query = 'update shf_renew_lic set approved_office=:decision, timestamp_office=now() where id=:id';
      $stm = $pdo->prepare($query);
      $stm->bindParam(':decision', $body['approvedOffice'], PDO::PARAM_STR);
      $stm->bindParam(':id', $args['id'], PDO::PARAM_STR);
      $stm->execute();
      // TODO check if the row existed, if not return 404
    }

    return $this->read($request, $response, $args)->withHeader('Cache-control', 'no-store');
  }

  /**
   * Execute a read operation, returns one element.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function read(Request $request, Response $response, $args) {
    $jwt = $this->auth->authenticateUser($request);
    $selectNames = $this->makeSqlSelectPart($this::KEYS);
    if(!empty($this::KEYS) || !empty($this::COLUMNS)) {
      $selectNames .= ',';
    }
    $selectNames .= $this->makeSqlSelectPart($this::COLUMNS);
    $condition = $this->makeSqlNameBinding($this::KEYS, ' and ');
    $query = 'select ' . $selectNames
           . ' from ' . $this::TABLE
           . ' where ' . $condition;
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    $this->bindValues($this::KEYS, $args, $stm);
    $stm->execute();
    $row = $stm->fetch();
    // check privileges
    if(!$row) {
      throw new HttpNotFoundException($request, 'No matching application found.');
    } else if(($row['licenseNbr'] != $jwt->lic) && !($jwt->privileges->clubadmin) &&!($jwt->privileges->office)) { // TODO clubadmin can only read club members
      throw new HttpForbiddenException($request, 'unautorized read request');
    }
    $payload = json_encode($row);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }

  /**
   * Execute a read operation, returns all elements.
   * Calls authorizeRead() to autorize the read operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function readAll(Request $request, Response $response, $args) {
    $this->authorizeReadAll($request, $args);
    $params = $request->getQueryParams();
    $licNbr = null;
    if(array_key_exists('licenseNbr', $params)) {
      $licNbr = $params['licenseNbr'];
    }
    $selectNames = $this->makeSqlSelectPart($this::KEYS);
    if(!empty($this::KEYS) || !empty($this::COLUMNS)) {
      $selectNames .= ',';
    }
    $selectNames .= $this->makeSqlSelectPart($this::COLUMNS);
    $query = 'select ' . $selectNames
           . ' from ' . $this::TABLE;
    if($licNbr) {
      $query .= ' where LicensNr=:lic';
    }
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    if($licNbr) {
      $stm->bindParam(':lic', $licNbr, PDO::PARAM_STR);
    }
    $stm->execute();
    $result = $stm->fetchAll();
    $payload = json_encode($result);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }

}
?>