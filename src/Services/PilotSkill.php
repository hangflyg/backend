<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Exception\HttpForbiddenException;

use \Holmby\CRUD\CRUD;

class PilotSkill extends CRUD {
  const TABLE = 'pilot_skills';
  const KEYS = array(
    'pilot_skill_id' => 'id'
  );
  const COLUMNS = array(
    'license_nbr' => 'licenseNbr',
    'issue_timestamp' => 'issueDate',
    'issuing_instructor_licnbr' => 'issuingInstructorLicNbr',
    'revoke_timestamp' => 'revokeDate',
    'skill_id' => 'skillId'
  );

  public function authorizeCreate(Request $request) {
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized create pilot skill');
  }

  public function authorizeRead(Request $request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized create pilot skill');
  }

  public function authorizeReadAll(Request $request, $args) {
    $jwt = $this->auth->authenticateUser($request);
    // office
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    // a pilot can allways read its own data
    $params = $request->getQueryParams();
    if($params['licenseNbr'] && $params['licenseNbr'] == $jwt->lic) {
      return;
    }
    throw new HttpForbiddenException($request, 'unautorized read request for pilot skills');
  }

  /**
   * Execute a read operation, returns all elements.
   * Calls authorizeRead() to autorize the read operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function readAll(Request $request, Response $response, $args) {
    $this->authorizeReadAll($request, $args);
    $query = 'select pilot_skill_id AS id,license_nbr AS licenseNbr,issue_timestamp AS issueDate,issuing_instructor_licnbr AS issuingInstructorLicNbr,revoke_timestamp AS revokeDate, skill_id AS skillId '
           . ' from pilot_skills where license_nbr=:lic and revoke_timestamp is null';
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    $params = $request->getQueryParams();
    $stm->bindParam(':lic', $params['licenseNbr']);
    $stm->execute();
    $result = $stm->fetchAll();
    $payload = json_encode($result);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }


}
?>