<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpForbiddenException;

use \Holmby\CRUD\CRUD;

class Club extends CRUD {
  const TABLE = 'shf_club';
  const KEYS = array(
    'id' => 'id'
  );
  const COLUMNS = array(
    'name' => 'name',
    'active' => 'active'
  );

  public function authorizeRead(Request $request, $args){
    return;
  }
  public function authorizeReadAll(Request $request, $args){
    return;
  }
}
?>