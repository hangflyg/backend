<?php
namespace SHF\API\Services;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpForbiddenException;

use \Holmby\CRUD\CRUD;

class Person extends CRUD {
  const TABLE = 'medlemmar';
  const COLUMNS = array(
    'PersonNr' => 'personNumber',
    'FNamn' => 'givenName',
    'ENamn' => 'familyName',
    'Adress' => 'street',
    'PostNr' => 'zip',
    'PostOrt' => 'city',
    'Land' => 'country',
    'TelMobil' => 'mobile',
    'email' => 'email',
    'hemlig' => 'protectedIdentity',
    'idrottsid' => 'iol',
    'AnhNamn' => 'contactPersonName',
    'AnhTel' => 'contactPersonPhone'
  );

  public function authorizeCreate(Request $request){
    throw new HttpForbiddenException($request, 'unautorized create request');
  }

  public function authorizeRead(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    // office
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    // own data
    if($jwt->sub != $args['id']) {
      throw new HttpForbiddenException($request, 'Unautorized read request. You do not have privileges to read person ' . $args['id'] . '.');
    }
  }

  public function authorizeReadAll(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    if($jwt->privileges->clubadmin) {
      return;
    }
    // office
    if(property_exists($jwt->privileges, 'office')) {
      return;
    }
    throw new HttpForbiddenException($request, 'Unautorized read request. You do not have privileges to read people.');
  }

  public function authorizeDelete(Request $request, $args){
    $jwt = $this->auth->authenticateUser($request);
    if($jwt->sub != $args['id']) {
      throw new HttpForbiddenException($request, 'Unautorized delete request. You do not have privileges to delete person ' . $args['id'] . '.');
    }
  }

  public function authorizeReplace(Request $request, $args){
return;
    $jwt = $this->auth->authenticateUser($request);
    if($jwt->sub != $args['id']) {
      throw new HttpForbiddenException($request, 'Unautorized replace request. You do not have privileges to update person ' . $args['id'] . '.');
    }
  }
}
?>