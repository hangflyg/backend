<?php
namespace Shf\API;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Server\RequestHandlerInterface as RequestHandler;

use \Slim\Exception\HttpBadRequestException;

class DecodeJsonMiddleware
{
  public function __invoke(Request $request, RequestHandler $handler): Response {
    $contentType = $request->getHeaderLine('Content-Type');
    if (strstr($contentType, 'application/json')) {
      $contents = json_decode(file_get_contents('php://input'), true);
      if (json_last_error() === JSON_ERROR_NONE) {
        $request = $request->withParsedBody($contents);
      } else {
        throw new HttpBadRequestException($request, 'Error parsing json body');
      }
    }
    return $handler->handle($request);
  }
}
?>