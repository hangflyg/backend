-- 2019-12-01
alter table shf_renew_lic add downgrade_elev boolean not null default false comment'pilotlicens som nergraderas till elev tills flygtiden uppfylls';
alter table shf_renew_lic add timestamp_instructor timestamp NULL DEFAULT NULL COMMENT 'tidstämpel, när instruktör godkänner ansökan efter uppflygning';
alter table shf_renew_lic add need_flight_test boolean NOT NULL DEFAULT false COMMENT 'sant om uppflygning för instruktör behövs som del av förnyelsen';

alter table shf_renew_lic add approved_club boolean DEFAULT NULL COMMENT 'true/false om klubben godkänt/avslagit ansökan';
update shf_renew_lic set approved_club = true where decision_club='approve';
update shf_renew_lic set approved_club = false where decision_club='reject';

alter table shf_renew_lic add approved_office boolean DEFAULT NULL COMMENT 'true/false om kansliet godkänt/avslagit ansökan';
update shf_renew_lic set approved_office = true where decision_office='approve';
update shf_renew_lic set approved_office = false where decision_office='reject';