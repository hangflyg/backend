# Install

```
git clone git@gitlab.com:hangflyg/backend.git
composer install --prefer-install=auto
```

# Run
ng build --watch
http://shf.localhost/shf/app/#/

# Update dependencies
```
composer outdated
composer update slim/slim
```